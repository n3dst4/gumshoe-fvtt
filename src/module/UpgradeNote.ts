import { seenPopup, systemName, templatesPath } from "../constants";

export class UpgradeNote extends FormApplication {
  /** @override */
  static get defaultOptions () {
    return mergeObject(super.defaultOptions, {
      classes: [systemName, "upgrade-note"],
      template: `${templatesPath}/upgrade-note.handlebars`,
      title: "An important note about the future",
      width: 660,
      // height: 660,
      // top: 100,
      // left: 100,
    });
  }

  activateListeners (html: JQuery) {
    html.find(".close").on("click", (e) => {
      e.preventDefault();
      this.close();
    });
    const gotcha = html.find<HTMLInputElement>(".gotcha");
    const checked = game.settings.get(systemName, seenPopup);
    gotcha.prop("checked", checked);
    gotcha.on("change", function (e) {
      game.settings.set(systemName, seenPopup, this.checked);
    });
  }

  getData () {
    return {
      isGM: game.user.isGM,
    };
  }
}

export const upgradeNoteInstance = new UpgradeNote({}, {});
